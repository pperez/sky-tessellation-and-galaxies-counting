package org.mdp.hadoop.cli;

public class SliceDivision implements SkyDivision{

	public static int ZONES = 20;
	double radsByZone = 2*Math.PI/ZONES;
	@Override
	public int idZone(double ra, double dec) {
		//select zone
		int zone = (new Double(ra/radsByZone)).intValue();
		return zone;
	}

	@Override
	public String description(int idZone) {
		return "zone "+idZone;
	}

}
