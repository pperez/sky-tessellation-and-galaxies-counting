package org.mdp.hadoop.cli;

public interface SkyDivision {

	public int idZone(double ra, double dec);
	public String description(int idZone);
	
}
