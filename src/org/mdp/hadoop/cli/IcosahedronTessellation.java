package org.mdp.hadoop.cli;

import java.util.HashMap;


public class IcosahedronTessellation implements SkyDivision{
	private HashMap<Integer, Triangle> triangles;
	private HashMap<Integer, Double[]> vertices;
	private MathUtilities mu;
	private double threshold;
	
	public IcosahedronTessellation(double threshold_){
		triangles = new HashMap<Integer, Triangle>();
		vertices = new HashMap<Integer, Double[]>();	
		mu = new MathUtilities();
		threshold = threshold_;
		setVertices();
		setTriangles();

	}
	
	private void setVertices(){
		
		double theta = 26.5650511770779*Math.PI/180;
		
		double stheta = Math.sin(theta);
		double ctheta = Math.cos(theta);
		
		vertices.put(0, new Double[]{ 0.0, 0.0, -1.0});//The lower vertex
		
		double phi;
		//The lower pentagon
		phi= Math.PI/5.0;
		lowerPentagon(phi, stheta, ctheta);
		
		//The upper pentagon
		phi = 0.0;
		upperPentagon(phi, stheta, ctheta);
		
		vertices.put(11, new Double[]{0.0, 0.0, 1.0});
	}
	
	
	private void lowerPentagon(double phi_, double stheta_, double ctheta_){
		
		for (int i=1; i<6; ++i){
			
			vertices.put(i, new Double[]{ctheta_*Math.cos(phi_), ctheta_*Math.sin(phi_), -stheta_});
			phi_+=2.0*Math.PI/5.0;
		}
		
	}
	
	private void upperPentagon(double phi_, double stheta_, double ctheta_){
		
		for (int i=6; i<11; ++i){
			vertices.put(i, new Double[]{ctheta_*Math.cos(phi_), ctheta_*Math.sin(phi_), stheta_});
			phi_+=2.0*Math.PI/5.0;
		}
	}
	
	
	private void setTriangles(){
		
		triangles.put(0, new Triangle(vertices.get(0), vertices.get(2), vertices.get(1)));
		triangles.put(1, new Triangle(vertices.get(0), vertices.get(3), vertices.get(2)));
		triangles.put(2, new Triangle(vertices.get(0), vertices.get(4), vertices.get(3)));
		triangles.put(3, new Triangle(vertices.get(0), vertices.get(5), vertices.get(4)));		
		triangles.put(4, new Triangle(vertices.get(0), vertices.get(1), vertices.get(5)));
		
		
		triangles.put(5, new Triangle(vertices.get(1), vertices.get(2), vertices.get(7)));
		triangles.put(6, new Triangle(vertices.get(2), vertices.get(3), vertices.get(8)));
		triangles.put(7, new Triangle(vertices.get(3), vertices.get(4), vertices.get(9)));	
		triangles.put(8, new Triangle(vertices.get(4), vertices.get(5), vertices.get(10)));
		triangles.put(9, new Triangle(vertices.get(5), vertices.get(1), vertices.get(6)));
		
		triangles.put(10, new Triangle(vertices.get(1), vertices.get(7), vertices.get(6)));
		triangles.put(11, new Triangle(vertices.get(2), vertices.get(8), vertices.get(7)));
		triangles.put(12, new Triangle(vertices.get(3), vertices.get(9), vertices.get(8)));
		triangles.put(13, new Triangle(vertices.get(4), vertices.get(10), vertices.get(9)));
		triangles.put(14, new Triangle(vertices.get(5), vertices.get(6), vertices.get(10)));
		
		triangles.put(15, new Triangle(vertices.get(6), vertices.get(7), vertices.get(11)));
		triangles.put(16, new Triangle(vertices.get(7), vertices.get(8), vertices.get(11)));
		triangles.put(17, new Triangle(vertices.get(8), vertices.get(9), vertices.get(11)));
		triangles.put(18, new Triangle(vertices.get(9), vertices.get(10), vertices.get(11)));
		triangles.put(19, new Triangle(vertices.get(10), vertices.get(6), vertices.get(11)));
		
	}
	
	
	@Override
	public int idZone(double ra, double dec) {
		// TODO Auto-generated method stub
		int i =0;
		Triangle t;
		while (i<20){
			t=triangles.get(i);
			double sumangles =mu.pointInTriangle(t, mu.radians2cartesian(ra, dec));
			if (sumangles>=threshold) break;
			
			i++;
		}
		if (i==20) i=-1;
		
		return i;
	}

	@Override
	public String description(int idZone) {
		// TODO Auto-generated method stub
		return "zone "+idZone;
	}
	
}
