package org.mdp.hadoop.cli;

public class Triangle {

	private  Double [][] v;
	private int spiral;
	private int elliptical;
	private int uncertain;
	
	public Triangle(Double [] v1, Double [] v2, Double [] v3 ){
		spiral=elliptical=uncertain=0;
		v = new Double [3][3];
		v[0] = v1;
		v[1] = v2;
		v[2] = v3;
			
 	}
	
	public void setVertices(Double [][] v_){
		
		v[0] = v_[0];
		v[1] = v_[1];
		v[2] = v_[2];
	}
	
	public Double[] getVerticeIndex(int i) throws Exception{
		
		if(i >= 3 ) throw new Exception(); 
		return this.v[i];
		
	}
	
	public void addSpiral(){
		spiral++;	
	}
		
	public void addElliptical(){
		elliptical++;
	}
	
	public void addUncertain(){
		uncertain++;
	}
	
	public int getSpiral(){
		return spiral;
	}
	
	public int getElliptical(){
		return elliptical;
	}
	
	public int getUncertain(){
		return uncertain;
	}
	
	public String toString(){
		return null;
		
	}
}
