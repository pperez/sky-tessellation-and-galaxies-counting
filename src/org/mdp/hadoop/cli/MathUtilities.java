package org.mdp.hadoop.cli;

public class MathUtilities {

		public MathUtilities(){}
		
		public double ra_hours2radians(double hh, double mm, double ss){
			
			return hh*Math.PI/12 + mm*Math.PI/720 + ss*Math.PI/43200;
		}
		
		
		public double dec_deg2radians(double dd, double mm, double ss){
			
			return dd*Math.PI/180+(mm/60)*Math.PI/180+(ss/3600)*Math.PI/180;
		}
		
		
		public double rad2sec(double var){
			
			return var*43200/Math.PI;
		}
		
		
		public double [] radians2cartesian(double phi, double theta){
			
			//phi : RA
			//theta : DEC
			double [] coord = new double [3];
			coord [0] = Math.cos(theta)*Math.cos(phi); 
			coord [1] = Math.cos(theta)*Math.sin(phi); 
			coord [2] = Math.sin(theta);	
			
			return coord;
			
		}
		
		public double vectorMod(double [] p){
			
			return Math.sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]);
			
		}
		
		
		public double pointInTriangle(Triangle t, double [] point){
			
			double epsilon= 0.0000001;
			double twopi=2*Math.PI; 
//			double rtod= 57.2957795;
			double anglesum =0;
			double pointProd=0;
			double [] p1 = new double [3];
			double [] p2 = new double [3];
			Double[] v1 = new Double[3];
			Double[] v2 = new Double[3];
			
			/*** Complete here***/
			for(int i=0; i<3; i++){
				
				try{
				v1 = t.getVerticeIndex(i);
				v2 = t.getVerticeIndex((i+1)%3);
				}catch (Exception e){
					
				}
				
				p1[0] =v1[0]-point[0];
				p1[1] =v1[1]-point[1];
				p1[2] =v1[2]-point[2];
				
				p2[0] =v2[0]-point[0];
				p2[1] =v2[1]-point[1];
				p2[2] =v2[2]-point[2];
				
			}
			
			double m1 = vectorMod(p1);
			double m2 = vectorMod(p2);
			
			if (m1*m2 <= epsilon) return twopi; //The point is a vertice. It's considered inside
			else {
				pointProd = p1[0]*p2[0]+p1[1]*p2[1]+p1[2]*p2[2];
				anglesum +=Math.acos(pointProd/(m1*m2));
			}
			return anglesum;
			
		}
		//public double [] cartesians2radec(double x, double y, double z){
			
		//	double [] radec = new double[2];
		//	radec[1] = Math.atan(z/x);//dec
		//	radec[0] = Math.atan(y/x);
		//}
}
