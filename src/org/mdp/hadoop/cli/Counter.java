package org.mdp.hadoop.cli;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;


/**
 * Java class to run a remote Hadoop word count job.
 * 
 * Contains the main method, an inner Reducer class 
 * and an inner CounterMapper class.
 * 
 */
public class Counter {
	
	/**
	 * Use this with line.split(SPLIT_REGEX) to get fairly nice
	 * word splits.
	 */
	public static final String SPLIT_REGEX = ",";
	public static MathUtilities util;
	static double threshold;

	static public final int SLICE = 1;
	static public final int TECELATION = 2;

	public static class CounterMapper extends MapReduceBase 
	implements Mapper<Object,Text,Text,IntWritable>{

		private final IntWritable one = new IntWritable(1);
		private Text text = new Text();
		MathUtilities mu = new MathUtilities();
		int type=SLICE;
		int threshold = 1;
		@Override
	    public void configure(JobConf job) {
	        super.configure(job);
	        type = job.getInt("type", SLICE);
	        threshold = job.getInt("threshold", 1);
		}
		@Override
		public void map(Object key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			
			String[] parts = value.toString().split(SPLIT_REGEX);
			String ra = parts[1].trim();
			String dec = parts[2].trim();
			boolean isSpiral = parts[13].trim().equals("1");
			boolean isElipt = parts[14].trim().equals("1");
			boolean isUncert = parts[15].trim().equals("1");
		    
			parts = ra.split(":");
			//RA : HH:MM:SS.ss
			double ra_rad=mu.ra_hours2radians(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]),Double.parseDouble(parts[2]));
			//DEC: DD°MM'SS.ss''
			parts = dec.split(":");
			double dec_rad = mu.dec_deg2radians(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]),Double.parseDouble(parts[2]));
			
			SkyDivision div=null;
			if(type==SLICE){
				div = new SliceDivision();
			}else{
				div = new IcosahedronTessellation(threshold);
			}
			
			int idZone = div.idZone(ra_rad, dec_rad);

			if(isSpiral){
				text.set(String.format("%s_spiral", div.description(idZone)));
				output.collect(text,one);
			}else if(isElipt){
				text.set(String.format("%s_eliptical", div.description(idZone)));
				output.collect(text,one);
			}else if(isUncert){
				text.set(String.format("%s_uncertain", div.description(idZone)));
				output.collect(text,one);
			}
			
		}
	
	}

	/**
	 * This is the Reducer Class.
	 * 
	 * This collects sets of key-value pairs with the same key on one machine. 
	 * 
	 * Remember that the generic is Reducer<MapKey, MapValue, OutputKey, OutputValue>
	 * 
	 * MapKey will be Text: a word from the file
	 * 
	 * MapValue will be IntWritable: a count: emit 1 for each occurrence of the type of galaxy in one zone 
	 * 
	 * OutputKey will be Text: the same type of galaxy and zone
	 * 
	 * OutputValue will be IntWritable: the final count
	 * 
	 *
	 */
	public static class CounterReducer extends MapReduceBase
		implements Reducer<Text,IntWritable,Text,IntWritable>{

		@Override
		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			int s = 0;
			while(values.hasNext()){
				s+=values.next().get();
			}
			output.collect(key, new IntWritable(s));
			
		}

	}

	/**
	 * Main method that sets up and runs the job
	 * 
	 * @param args First argument is input, second is output
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		
		JobClient client = new JobClient();
		JobConf conf = new JobConf(Counter.class);
		
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);
		
		FileInputFormat.addInputPath(conf,new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));
		conf.setMapperClass(CounterMapper.class);
		
		int typeofdiv = SLICE;
		if(args.length>3 && args[2].matches("-t")){
			typeofdiv = TECELATION;
			conf.setInt("threshold", Integer.parseInt(args[3]));
		}
		conf.setInt("type",typeofdiv);
		
		conf.setReducerClass(CounterReducer.class);
		client.setConf(conf);
		try{
			JobClient.runJob(conf);
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
	
}
