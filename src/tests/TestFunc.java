package tests;
import org.mdp.hadoop.cli.*;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class TestFunc {
	
	MathUtilities mu;
	IcosahedronTessellation it;
	Triangle t;
	double theta, phi;
	
	@Before
	public void setUp(){

		theta = 26.5650511770779*Math.PI/180;
		phi= Math.PI/5.0;
		
		t= new Triangle(new Double[]{0.0, 0.0, -1.0}, 
				        new Double[]{Math.cos(theta)*Math.cos(phi), Math.cos(theta)*Math.sin(phi), -Math.sin(theta)},
				        new Double[]{Math.cos(theta)*Math.cos(phi+2.0*Math.PI/5.0), Math.cos(theta)*Math.sin(phi+2.0*Math.PI/5.0), -Math.sin(theta)}
);

		mu = new MathUtilities();
	}
	
	@Test
	public void testIsPoint(){
		double [] p = {0.0, 0.0, -1.0};
		assertTrue(mu.pointInTriangle(t, p)== 2*Math.PI);
		
		double [] p2 = {Math.cos(2*theta/3)*Math.cos(phi+Math.PI/5.0), Math.cos(2*theta/3)*Math.sin(phi+Math.PI/5.0), -Math.sin(2*theta/3)};
		System.out.println(mu.pointInTriangle(t, p2));
		
	}
}
